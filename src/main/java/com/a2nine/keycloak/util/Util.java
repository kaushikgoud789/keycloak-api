package com.a2nine.keycloak.util;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response.StatusType;

import org.springframework.stereotype.Component;

@Component
public class Util {

	public static Date getFormattedDate(Date date) {
		long batch_date = date.getTime();
		Date dt = new Date(batch_date * 1000);

		SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy");
		return new Date(sfd.format(dt));

	}
	
	 public static String getCreatedId(Response response) {
	        URI location = response.getLocation();
	        if (!response.getStatusInfo().equals(Status.CREATED)) {
	            StatusType statusInfo = response.getStatusInfo();
	            throw new WebApplicationException("Create method returned status " +
	                    statusInfo.getReasonPhrase() + " (Code: " + statusInfo.getStatusCode() + "); expected status: Created (201)", response);
	        }
	        if (location == null) {
	            return null;
	        }
	        String path = location.getPath();
	        return path.substring(path.lastIndexOf('/') + 1);
	    }

}
