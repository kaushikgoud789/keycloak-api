package com.a2nine.keycloak.rest;

import static com.a2nine.keycloak.util.ObjectSerializer.toJsonString;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import com.a2nine.keycloak.model.User;
import com.a2nine.keycloak.usecases.FindAndSaveUsers;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("/users")
public class UsersController {
	private final Logger log = LogManager.getLogger(UsersController.class);

	@Autowired
	Keycloak keycloakClient;

	@Autowired
	FindAndSaveUsers findAndSaveUsers;

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response createUser(@RequestBody String jsonBody) {
		try {
			findAndSaveUsers.createUser(new ObjectMapper().readValue(jsonBody, User.class));
			return Response.status(Response.Status.CREATED.getStatusCode()).build();
		} catch (Exception e) {
			log.error(e.getMessage());
			return Response.status(Response.Status.BAD_REQUEST.getStatusCode()).build();
		}

	}

	@GET
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getAllUsers() {
		try {
			return Response.status(Response.Status.OK.getStatusCode()).entity(toJsonString(findAndSaveUsers.getAllUsers())).build();
		} catch (JsonProcessingException e) {
			log.error(e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()).build();
		}
	}

	@GET
	@Path("/id")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response fetchUserById(@QueryParam("userId") String userId) {
		try {
			return Response.status(Response.Status.OK.getStatusCode()).entity(toJsonString(findAndSaveUsers.getUserById(userId))).build();
		} catch (JsonProcessingException e) {
			log.error(e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()).build();
		}

	}

	@POST
	@Path("/update")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateUserDetails(@RequestBody String jsonBody) {
		User user;
		try {
			findAndSaveUsers.updateUser(new ObjectMapper().readValue(jsonBody, User.class));
			return Response.status(Response.Status.OK.getStatusCode()).build();
		} catch (Exception e) {
			log.error(e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()).build();
		}
	}
	
	@GET
	@Path("/roles")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getRealmRoles() {
		try {
			return Response.status(Response.Status.OK.getStatusCode()).entity(toJsonString(findAndSaveUsers.getRealmRoles("AFS"))).build();
		} catch (JsonProcessingException e) {
			log.error(e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()).build();
		}
	}
	
	@GET
	@Path("/groups")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getRealmGroups() {
		try {
			return Response.status(Response.Status.OK.getStatusCode()).entity(toJsonString(findAndSaveUsers.getRealmGroups("AFS"))).build();
		} catch (JsonProcessingException e) {
			log.error(e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()).build();
		}
	}
	
	

}
