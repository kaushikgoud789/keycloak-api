package com.a2nine.keycloak.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.a2nine.keycloak.util.CORSFilter;
import com.a2nine.keycloak.rest.UsersController;

@Configuration
@ApplicationPath("/keycloak")
public class JerseyWebConfig extends ResourceConfig {

	public JerseyWebConfig() {
		register(UsersController.class);
		register(CORSFilter.class);
	}

}
