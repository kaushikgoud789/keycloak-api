package com.a2nine.keycloak.config;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.a2nine.keycloak.model.KeycloakProperties;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	private KeycloakProperties keycloakProperties;
	
	@Autowired
    public void setKeycloakProperties(KeycloakProperties keycloakProperties) {
        this.keycloakProperties = keycloakProperties;
    }
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/*").permitAll().and().csrf().disable();
	}
	
   /* The Keycloak Admin client that provides the service-account Access-Token
    *
    * @param props
    * @return
    */
   @Bean
   public Keycloak keycloak() {
       Keycloak keycloak = KeycloakBuilder.builder()
	            .serverUrl(keycloakProperties.getUrl())
	            .realm(keycloakProperties.getRealm())
	            .username(keycloakProperties.getUsername())
	            .password(keycloakProperties.getPassword())
	            .clientId(keycloakProperties.getClientId())
	            .resteasyClient(
	                new ResteasyClientBuilder().disableTrustManager().hostnameVerification(ResteasyClientBuilder.HostnameVerificationPolicy.ANY)
	                    .connectionPoolSize(10).build()
	            ).build();

       return keycloak;
   }
}