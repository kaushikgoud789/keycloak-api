package com.a2nine.keycloak.usecases;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.GroupsResource;
import org.keycloak.admin.client.resource.RoleMappingResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2nine.keycloak.model.User;
import com.a2nine.keycloak.util.Util;

@Service
public class FindAndSaveUsers {
	private final Logger log = LogManager.getLogger(FindAndSaveUsers.class);

	@Autowired
	Keycloak keycloakClient;

	@Autowired
	Util util;

	public void createUser(User userObj) {

		// Set user details
		UserRepresentation user = fillInUserRepresentation(new UserRepresentation(), userObj);

		// Set attributes
		Map<String, List<String>> attr = fillInUserAttributes(userObj);
		user.setAttributes(attr);

		// Create User
		Response createUserResponse = keycloakClient.realm("AFS").users().create(user);
		String errMsg = createUserResponse.readEntity(String.class);
		if (createUserResponse.getStatus() != 201) {
			if (createUserResponse.getStatus() == 409) {
				log.debug("creating user failed due to duplicated user" + errMsg);
			} else {
				log.debug("creating user failed, status: " + createUserResponse.getStatus() + ", " + errMsg);
			}
		}
		createUserResponse.close();
		String createdId = Util.getCreatedId(createUserResponse);
		System.out.printf("User created with id: %s%n", createdId);
		// Reset password
		resetPassword(createdId, userObj.password());

		// Add roles to user
		addRealmRolesToUser(createdId, userObj.roles());

		// Add organizations to user
		addRealmGroupsToUser(createdId, userObj.organization());
	}

	public void resetPassword(String userId, String password) {
		CredentialRepresentation passwordCred = new CredentialRepresentation();
		passwordCred.setTemporary(false);
		passwordCred.setValue(password);
		passwordCred.setType(CredentialRepresentation.PASSWORD);

		UserResource ur = keycloakClient.realm("AFS").users().get(userId);
		ur.resetPassword(passwordCred);
		System.out.printf("Password for user with id: %s reseted%n", userId);
	}

	public void addRealmRolesToUser(String userId, List rolenames) {
		List<RoleRepresentation> roles = keycloakClient.realm("AFS").users().get(userId).roles().realmLevel()
				.listAvailable().stream().filter(r -> rolenames.contains(r.getId())).collect(Collectors.toList());
		keycloakClient.realm("AFS").users().get(userId).roles().realmLevel().add(roles);
	}

	public void removeRealmRolesFromUser(String userId, List rolenames) {
		List<RoleRepresentation> roles = keycloakClient.realm("AFS").users().get(userId).roles().realmLevel()
				.listAvailable().stream().filter(r -> rolenames.contains(r.getId())).collect(Collectors.toList());
		keycloakClient.realm("AFS").users().get(userId).roles().realmLevel().remove(roles);
	}

	public void addRealmGroupsToUser(String userId, List groups) {
		List<GroupRepresentation> realmGrps = getRealmGroups("AFS").stream().filter(g -> groups.contains(g.getId()))
				.collect(Collectors.toList());
		realmGrps.forEach(grp -> {
			keycloakClient.realm("AFS").users().get(userId).joinGroup(grp.getId());
		});

	}

	public void removeRealmGroupsFromUser(String userId, List groups) {
		List<GroupRepresentation> realmGrps = getRealmGroups("AFS").stream().filter(g -> groups.contains(g.getId()))
				.collect(Collectors.toList());
		realmGrps.forEach(grp -> {
			keycloakClient.realm("AFS").users().get(userId).leaveGroup(grp.getId());
		});

	}

	public void createRoles(String userId) {
		RoleRepresentation userRealmRole = keycloakClient.realm("AFS").roles().get(userId).toRepresentation();
		keycloakClient.realm("AFS").users().get(userId).roles().realmLevel().add(Arrays.asList(userRealmRole));
		System.out.printf("Granted user realm role to user with id: %s%n", userId);
	}

	public List<UserRepresentation> getAllUsers() {
		List<UserRepresentation> users = keycloakClient.realm("AFS").users().list();
		// get each user realm roles and set roles in user object
		users.forEach(user -> {
			user.setRealmRoles(getRolesByUserId(user.getId()));
		});
		return users;
	}

	public List<String> getRolesByUserId(String id) {
		List<RoleRepresentation> realmRoles = keycloakClient.realm("AFS").users().get(id).roles().realmLevel()
				.listAll();
		// List<GroupRepresentation> test =
		// keycloakClient.realm("AFS").users().get(id).groups();
		return realmRoles.stream().map(RoleRepresentation::getName).collect(Collectors.toList());

	}

	public UserRepresentation getUserById(String id) {
		UserResource userResource = keycloakClient.realm("AFS").users().get(id);
		UserRepresentation userRepresentation = userResource.toRepresentation();
		userRepresentation.setRealmRoles(userResource.roles().getAll().getRealmMappings().stream()
				.map(RoleRepresentation::getId).collect(Collectors.toList()));
		userRepresentation.setGroups(
				userResource.groups().stream().map(GroupRepresentation::getId).collect(Collectors.toList()));
		return userRepresentation;
	}

	public List<RoleRepresentation> getRealmRoles(String realm) {
		return keycloakClient.realm("AFS").roles().list();
	}

	public List<GroupRepresentation> getUserGroups(String id) {
		return keycloakClient.realm("AFS").users().get(id).groups();
	}

	public List<GroupRepresentation> getRealmGroups(String realm) {
		return keycloakClient.realm(realm).groups().groups();
	}

	public UserRepresentation searchUser(String userId) {
		UserRepresentation user = keycloakClient.realm("AFS").users().search(userId).get(0);
		return user;
	}

	public void updateUser(User userObj) {
		// UserRepresentation ur = searchUser(userObj.username());
		UserRepresentation ur = getUserById(userObj.id());
		// Set user details
		UserRepresentation user = fillInUserRepresentation(ur, userObj);

		// Set attributes
		Map<String, List<String>> attr = fillInUserAttributes(userObj);
		user.setAttributes(attr);

		// TODO: Set roles and organizations
		
		List<String> tempNewRoles = userObj.roles();
		List<String> tempExisRoles = ur.getRealmRoles();
		
		List<String> existingRoles = ur.getRealmRoles();
		List<String> NewRoles = userObj.roles();
		
		System.out.println("Existing roles: "+existingRoles);
		System.out.println("Existing groups: "+ur.getGroups());
		
		System.out.println("New roles: "+userObj.roles());
		System.out.println("New groups: "+userObj.organization());

		//finding missing roles
		 //remove all elements from second list
		//tempNewRoles.removeAll(existingRoles);
		tempExisRoles.removeAll(NewRoles);
		System.out.println("Roles to be Removed: "+ tempExisRoles);
		
		tempNewRoles.removeAll(existingRoles);
		System.out.println("Roles to be Added: "+ tempNewRoles);
		
		//remove all roles from user and add all new user roles 
		//List<RoleRepresentation> realmRoles = keycloakClient.realm("AFS").users().get(userObj.id()).roles().realmLevel().listAll();
		
		//keycloakClient.realm("AFS").users().get(userObj.id()).roles().realmLevel().remove(realmRoles);

		//addRealmRolesToUser(userObj.id(), userObj.roles());
		
		// Add new roles to user
		//addRealmRolesToUser(userObj.id(), userObj.roles());

		// Add organizations to user
		//addRealmGroupsToUser(userObj.id(), userObj.organization());

		// Remove roles if any
		//removeRealmRolesFromUser(userObj.id(), userObj.organization());

		// Remove groups if any
		//removeRealmGroupsFromUser(userObj.id(), userObj.organization());

		// Update User
		keycloakClient.realm("AFS").users().get(userObj.id()).update(ur);
	}

	public Map<String, List<String>> fillInUserAttributes(User userObj) {
		Map<String, List<String>> attr = new HashMap<>();
		attr.put("departmentId", Collections.singletonList(userObj.department()));
		// attr.put("departmentName",
		// Collections.singletonList(userObj.departmentName()));
		attr.put("mobile", Collections.singletonList(userObj.mobile()));
		return attr;
	}

	public UserRepresentation fillInUserRepresentation(UserRepresentation user, User userObj) {
		user.setUsername(userObj.username());
		user.setEnabled(true);
		if (userObj.firstName() != null && !userObj.firstName().trim().isEmpty()) {
			user.setFirstName(userObj.firstName());
		}
		if (userObj.lastName() != null && !userObj.lastName().trim().isEmpty()) {
			user.setLastName(userObj.lastName());
		}
		if (userObj.email() != null && !userObj.email().trim().isEmpty()) {
			user.setEmail(userObj.email());
			user.setEmailVerified(true);
		}
		return user;
	}
}
