package com.a2nine.keycloak.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

	private String id;
	private String username;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String department;
	private String departmentName;
	private String mobile;
	private ArrayList organization;
	private ArrayList roles;
	private Map<String, List<String>> attributes;
	
	@JsonCreator
	public User(@JsonProperty("id") String id,@JsonProperty("username") String username, @JsonProperty("firstName") String firstName,
			@JsonProperty("lastName") String lastName, @JsonProperty("password") String password,
			@JsonProperty("email") String email, @JsonProperty("attributes") Map<String, List<String>> attributes,
			@JsonProperty("department") String department, @JsonProperty("departmentName") String departmentName, 
			@JsonProperty("mobile") String mobile, @JsonProperty("organization") String organization, 
			@JsonProperty("roles") ArrayList roles) {
		super();
		this.id = id;
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.email = email;
		this.attributes = attributes;
		this.department = department;
		this.departmentName = departmentName;
		this.mobile = mobile;
		this.organization = new ArrayList<>();
		this.organization.add(organization);
		this.roles = roles;
	}
	
	public String id() {
		return this.id;
	}
	
	public String username() {
		return this.username;
	}
	
	public String firstName() {
		return this.firstName;
	}

	public String lastName() {
		return this.lastName;
	}
	
	public String password() {
		return this.password;
	}

	public String email() {
		return this.email;
	}
	
	public Map<String, List<String>> attributes() {
		return this.attributes;
	}
	
	public String department() {
		return this.department;
	}
	
	public String departmentName() {
		return this.departmentName;
	}
	
	public String mobile() {
		return this.mobile;
	}
	
	public ArrayList organization() {
		return this.organization;
	}
	
	public ArrayList roles() {
		return this.roles;
	}

}
